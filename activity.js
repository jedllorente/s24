db.users.insertMany([{
        firstName: "Nikola",
        lastName: "Tesla",
        age: 64,
        department: "Communication",
    },
    {
        firstName: "Marie",
        lastName: "Curie",
        age: 28,
        department: "Media and Arts",
    },
    {
        firstName: "Charles",
        lastName: "Darwin",
        age: 51,
        department: "Human Resources",
    },
    {
        firstName: "Rene",
        lastName: "Descarites",
        age: 50,
        department: "Media and Arts",
    },
    {
        firstName: "Albert",
        lastName: "Einstein",
        age: 51,
        department: "Human Resources",
    },
    {
        firstName: "Michael",
        lastName: "Faraday",
        age: 30,
        department: "Communication",
    },

]);

/* Find users with letter s in firstname or d in lastname */
db.users.find({
    $or: [
        { firstName: { $regex: 's', $options: '$s' } },
        { lastName: { $regex: 'd', $options: '$d' } }
    ]
}, { _id: 0, firstName: 1, lastName: 1 })

/* Find users with letter e in firstname and has an age of less than or equal to 30 */
db.users.find({
    $and: [
        { firstName: { $regex: 'e', $options: '$e' } },
        { age: { $lte: 30 } }
    ]
}, {
    _id: 0,
    firstName: 1,
    age: 1
});